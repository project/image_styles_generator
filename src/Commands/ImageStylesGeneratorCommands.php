<?php

namespace Drupal\image_styles_generator\Commands;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\image_styles_generator\DerivativeWarmerInterface;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Helper\ProgressBar;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class ImageStylesGeneratorCommands extends DrushCommands {

  /**
   * Used to create / delete entities.
   *
   * @var \Drupal\image_styles_generator\DerivativeWarmerInterface
   */
  protected DerivativeWarmerInterface $derivativeWarmer;

  /**
   * Used to create / delete entities.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;
  
  /**
   * Construct the command.
   */
  public function __construct(DerivativeWarmerInterface $derivative_warmer, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct();
    $this->entityTypeManager = $entity_type_manager;
    $this->derivativeWarmer = $derivative_warmer;
  }

  /**
   * Generate all derivatives for all image styles.
   *
   * @todo only generate the neccesary image styles. For this,
   * join with the file usage table and load the content with
   * images, then for each field that has an image, get its image
   * styles from its display formatters information and generate
   * images only for those image styles.
   *
   * @command image_derivatives:generate
   *
   * @option image_styles
   *   Coma separated image styles IDs.
   *
   * @usage image_derivatives:generate
   */
  public function generateAllDerivativesForAllImagesInDatabase($options = ['image_styles' => NULL]) {
    $image_styles_ids = $options['image_styles'] ? explode(',', $options['image_styles']) : $options['image_styles'];

    try {
      $image_styles = $this->loadImageStyles($image_styles_ids);
      $fids = $this->loadAllImageFiles();
    }
    catch (PluginNotFoundException | InvalidPluginDefinitionException $e) {
      $this->logger->error($e->getMessage());
      return;
    }
    $this->output()->writeln('Found ' . count($image_styles) . ' image styles and ' . count($fids) . ' images to generate.');
    $progress_bar = $this->initializeProgressBar(count($image_styles) * count($fids));
    foreach ($fids as $fid) {
      $file = $this->entityTypeManager->getStorage('file')->load($fid);
      foreach ($image_styles as $image_style) {
        $this->derivativeWarmer->regenerateImageStyleDerivativeFromFile($image_style, $file);
        $progress_bar->advance();
      }
    }
    $progress_bar->finish();
    $this->output()->writeln('All derivatives have been generated for all image styles.');
  }

  /**
   * Load all image files from database.
   *
   * @return array
   *   Returns array of file ids.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadAllImageFiles(): array {
    $query = $this->entityTypeManager->getStorage('file')->getQuery()->accessCheck();
    $query->condition('filemime', 'image%', 'LIKE');
    $query->condition('status', 1);
    $query->sort('fid', 'DESC');
    return $query->execute();
  }

  /**
   * Load image passed or all image style objects.
   *
   * @param array|null $image_styles_ids
   *   Array of image styles ids.
   *
   * @return array
   *   Returns an array of image style objects found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadImageStyles(array|null $image_styles_ids = NULL): array {
    return $this->entityTypeManager->getStorage('image_style')
      ->loadMultiple($image_styles_ids);
  }

  /**
   * Initializes progressBar object.
   *
   * @param string $total
   *   Total number of items to process.
   *
   * @return \Symfony\Component\Console\Helper\ProgressBar
   *   Returns a progress bar object.
   */
  public function initializeProgressBar(string $total): ProgressBar {
    $progress_bar = new ProgressBar($this->output, $total);
    $progress_bar->setFormat('very_verbose');
    return $progress_bar;
  }

}
